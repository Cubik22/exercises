#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return M_PI * (3 * (_a + _b) - sqrt((3 * _a + _b) * (_a + 3 * _b)));
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
    points.reserve(3);
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    unsigned int np = points.size();
    for (unsigned int i = 0; i < np; i++){
        perimeter += (points[(i + 1) % np] - points[i % np]).ComputeNorm2();
    }
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                    const double& edge) : Triangle(p1, {p1.X + edge, p1.Y}, {p1.X + (1.0/2) * edge, p1.Y + (sqrt(3)/2.0) * edge}) {}



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
    points.reserve(4);
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    unsigned int np = points.size();
    for (unsigned int i = 0; i < np; i++){
        perimeter += (points[(i + 1) % np] - points[i % np]).ComputeNorm2();
    }
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height) : Quadrilateral(p1, {p1.X + base, p1.Y}, {p1.X + base, p1.Y + height}, {p1.X, p1.Y + height})
  {}

  Point Point::operator+(const Point& point) const
  {
      return Point(X + point.X, Y + point.Y);
  }

  Point Point::operator-(const Point& point) const
  {
    return Point(X - point.X, Y - point.Y);
  }

  Point& Point::operator-=(const Point& point)
  {
    X -= point.X;
    Y -= point.Y;
    return *this;
  }

  Point& Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }
}
