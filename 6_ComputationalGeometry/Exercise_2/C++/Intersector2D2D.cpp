#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = TypeIntersection::NoInteresection;
}


Intersector2D2D::~Intersector2D2D()
{}

// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    bool intersection = false;
    tangentLine = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
    if (tangentLine.norm() >= toleranceIntersection){
        matrixNomalVector.row(2) = tangentLine;
        rightHandSide(2) = 0.0;
        pointLine = matrixNomalVector.colPivHouseholderQr().solve(rightHandSide);
        intersectionType = TypeIntersection::LineIntersection;
        intersection = true;
    } else{
        if (abs(rightHandSide(0) - rightHandSide(1)) < toleranceParallelism){
            intersectionType = TypeIntersection::Coplanar;
        } else{
            intersectionType = TypeIntersection::NoInteresection;
        }
    }
    return intersection;
}
