#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = TypeIntersection::NoInteresection;
}

Intersector2D1D::~Intersector2D1D()
{}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = planeNormal;
    planeTranslationPointer = planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = lineOrigin;
    lineTangentPointer = lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    bool intersection = false;
    double denominator = planeNormalPointer.dot(lineTangentPointer);
    if (abs(denominator) >= toleranceIntersection){
        double numerator = planeTranslationPointer - planeNormalPointer.dot(lineOriginPointer);
        intersectionParametricCoordinate = numerator / denominator;
        intersectionType = TypeIntersection::PointIntersection;
        intersection = true;
    } else{
        if (abs(planeNormalPointer.dot(lineOriginPointer)) < toleranceParallelism){
            intersectionType = TypeIntersection::Coplanar;
        } else{
            intersectionType = TypeIntersection::NoInteresection;
        }
    }
    return intersection;
}
