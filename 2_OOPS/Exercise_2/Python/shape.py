import math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a = a
        self.b = b

    def area(self) -> float:
        return math.pi * self.a * self.b

class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def area(self) -> float:
        return (1.0 / 2) * abs(self.p1.x * self.p2.y + self.p2.x * self.p3.y + self.p3.x * self.p1.y -
                               self.p2.x * self.p1.y - self.p3.x * self.p2.y - self.p1.x * self.p3.y);


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, Point(p1.x + edge, p1.y), Point(p1.x + edge/2.0, p1.y + (math.sqrt(3)/2.0) * edge))


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4

    def area(self) -> float:
        return (1.0 / 2) * abs(self.p1.x * self.p2.y + self.p2.x * self.p3.y + self.p3.x * self.p4.y + self.p4.x * self.p1.y -
                               self.p2.x * self.p1.y - self.p3.x * self.p2.y - self.p4.x * self.p3.y - self.p1.x * self.p4.y);


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, Point(p2.x + (p4.x - p1.x), p4.y), p4)


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        super().__init__(p1, Point(p1.x + base, p1.y), Point(p1.x, p1.y + height))


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)
