#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      const double x;
      const double y;
      Point(const double& _x, const double& _y);
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    private:
      Point center;
      int a;
      int b;

    public:
      Ellipse(const Point& _center, const int& _a, const int& _b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center, const int& radius);
  };


  class Triangle : public IPolygon
  {
    private:
      Point p1;
      Point p2;
      Point p3;

    public:
      Triangle(const Point& _p1, const Point& _p2, const Point& _p3);

      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p, const int& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point p1;
      Point p2;
      Point p3;
      Point p4;

    public:
      Quadrilateral(const Point& _p1, const Point& _p2, const Point& _p3, const Point& _p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& _p1, const Point& _p2, const Point& _p4);
  };

  class Rectangle : public Parallelogram
  {
    public:
      Rectangle(const Point& _p1, const int& base, const int& height);
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& _p1, const int& edge);
  };
}

#endif // SHAPE_H
