#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &_x, const double &_y) : x{_x}, y{_y} {}

Point::Point(const Point &point) : x{point.x}, y{point.y} {}

Ellipse::Ellipse(const Point &_center, const int &_a, const int &_b) : center{_center}, a{_a}, b{_b} {}

double Ellipse::Area() const
{
    return M_PI * a * b;
}

Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius) {}

Triangle::Triangle(const Point &_p1, const Point &_p2, const Point &_p3) : p1{_p1}, p2{_p2}, p3{_p3} {}

double Triangle::Area() const
{
    return (1.0/2) * abs(p1.x * p2.y + p2.x * p3.y + p3.x * p1.y -
                         p2.x * p1.y - p3.x * p2.y - p1.x * p3.y);
}

TriangleEquilateral::TriangleEquilateral(const Point &p, const int &edge) :
    Triangle(p, {p.x + edge, p.y}, {p.x + edge/2.0, p.y + (sqrt(3)/2.0) * edge}) {}

Quadrilateral::Quadrilateral(const Point &_p1, const Point &_p2, const Point &_p3, const Point &_p4) : p1{_p1}, p2{_p2}, p3{_p3}, p4{_p4} {}

double Quadrilateral::Area() const
{
    return (1.0/2) * abs(p1.x * p2.y + p2.x * p3.y + p3.x * p4.y + p4.x * p1.y -
                         p2.x * p1.y - p3.x * p2.y - p4.x * p3.y - p1.x * p4.y);
}

Parallelogram::Parallelogram(const Point &_p1, const Point &_p2, const Point &_p4) :
    Quadrilateral(_p1, _p2, {_p2.x + (_p4.x - _p1.x), _p2.y + (_p4.y - _p1.y)}, _p4) {}

Rectangle::Rectangle(const Point &_p1, const int &base, const int &height) : Parallelogram(_p1, {_p1.x + base, _p1.y}, {_p1.x, _p1.y + height}) {}

Square::Square(const Point &_p1, const int &edge) : Rectangle(_p1, edge, edge) {}

}
