class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.Ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        if (ingredient in self.Ingredients):
            raise Exception("Ingredient already inserted")
        self.Ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.Ingredients)

    def computePrice(self) -> int:
        price = 0
        for ingre in self.Ingredients:
            price += ingre.Price
        return price

class Order:
    def __init__(self, numPizzas: int):
        self.initializeOrder(numPizzas)

    def getPizza(self, position: int) -> Pizza:
        if position >= self.numPizzas():
            raise Exception("Position passed is wrong")
        return self.Pizzas[position]

    def initializeOrder(self, numPizzas: int):
        if numPizzas == 0:
            raise Exception("Empty order")
        self.Pizzas = []

    def addPizza(self, pizza: Pizza):
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.Pizzas)

    def computeTotal(self) -> int:
        total = 0
        for pizza in self.Pizzas:
            total += pizza.computePrice()
        return total


def takeName(ingredient):
    return ingredient.Name

class Pizzeria:
    numOrder: int = 1000

    def __init__(self):
        self.Ingredients = []
        self.Pizzas = []
        self.Orders = {}

    def addIngredient(self, name: str, description: str, price: int):
        for ingre in self.Ingredients:
            if ingre.Name == name:
                raise Exception("Ingredient already inserted")
        self.Ingredients.append(Ingredient(name, price, description))

    def findIngredient(self, name: str) -> Ingredient:
        for ingre in self.Ingredients:
            if ingre.Name == name:
                return ingre
        raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        for pizza in self.Pizzas:
            if pizza.Name == name:
                raise Exception("Pizza already inserted")
        pizza = Pizza(name)
        for ingre in ingredients:
            pizza.addIngredient(self.findIngredient(ingre))
        self.Pizzas.append(pizza)

    def findPizza(self, name: str) -> Pizza:
        for pizza in self.Pizzas:
            if pizza.Name == name:
                return pizza
        raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        order = Order(len(pizzas))
        for pizza in pizzas:
            order.addPizza(self.findPizza(pizza))
        self.Orders[Pizzeria.numOrder] = order
        Pizzeria.numOrder += 1
        return Pizzeria.numOrder - 1

    def findOrder(self, numOrder: int) -> Order:
        if numOrder not in self.Orders.keys():
            raise Exception("Order not found")
        return self.Orders[numOrder]

    def getReceipt(self, numOrder: int) -> str:
        receipt: str = ""
        order: Order = self.findOrder(numOrder)
        for pizza in order.Pizzas:
            receipt += "- " + pizza.Name + ", " + str(pizza.computePrice()) + " euro\n"
        receipt += "  TOTAL: " + str(order.computeTotal()) + " euro\n"
        return receipt

    def listIngredients(self) -> str:
        list: str = ""
        self.Ingredients.sort(key=takeName)
        for ingre in self.Ingredients:
            list += ingre.Name + " - '" + ingre.Description + "': " + str(ingre.Price) + " euro\n"
        return list

    def menu(self) -> str:
        men: str = ""
        for pizza in self.Pizzas:
            men += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(pizza.computePrice()) + " euro\n"
        return men
