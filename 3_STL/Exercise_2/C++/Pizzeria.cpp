#include "Pizzeria.h"

namespace PizzeriaLibrary {

Ingredient::Ingredient(const string &name, const string &description, const unsigned int &price) : Name{name}, Description{description}, Price{price} {}

Pizza::Pizza(const string& name, unsigned int size) : Name{name} {
    Ingredients.reserve(size);
}

void Pizza::AddIngredient(const Ingredient &ingredient) {
    Ingredients.push_back(ingredient);
}

unsigned int Pizza::NumIngredients() const {
    return Ingredients.size();
}

unsigned int Pizza::ComputePrice() const {
    unsigned int price = 0;
    for (unsigned int i = 0; i < NumIngredients(); i++){
        price += Ingredients[i].Price;
    }
    return price;
}

void Order::InitializeOrder(unsigned int numPizzas) {
    if (numPizzas == 0){
        throw runtime_error("Empty order");
    }
    Pizzas.reserve(numPizzas);
}

void Order::AddPizza(const Pizza &pizza) {
    Pizzas.push_back(pizza);
}

const Pizza &Order::GetPizza(const unsigned int &position) const {
    if (position >= Pizzas.size()){
        throw runtime_error("Position passed is wrong");
    }
    return Pizzas[position];
}

unsigned int Order::NumPizzas() const {
    return Pizzas.size();
}

unsigned int Order::ComputeTotal() const {
    unsigned int total = 0;
    for (unsigned int i = 0; i < NumPizzas(); i++){
        total += Pizzas[i].ComputePrice();
    }
    return total;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {
    for (list<Ingredient>::iterator it = Ingredients.begin(); it != Ingredients.end(); it++){
        const Ingredient& ingredient = *it;
        if (ingredient.Name == name){
            throw runtime_error("Ingredient already inserted");
        } else if (ingredient.Name > name){
            Ingredients.emplace(it, name, description, price);
            return;
        }
    }
    Ingredients.emplace_back(name, description, price);
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const {
    for (list<Ingredient>::const_iterator it = Ingredients.begin(); it != Ingredients.end(); it++){
        const Ingredient& ingredient = *it;
        if (ingredient.Name == name){
            return ingredient;
        } else if (ingredient.Name > name){
            throw runtime_error("Ingredient not found");
        }
    }
    throw runtime_error("Ingredient not found");
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {
    for (unsigned int i = 0; i < Pizzas.size(); i++){
        if (name == Pizzas[i].Name){
            throw runtime_error("Pizza already inserted");
        }
    }
    Pizza pizza = Pizza(name, ingredients.size());
    for (unsigned int i = 0; i < ingredients.size(); i++){
        pizza.AddIngredient(FindIngredient(ingredients[i]));
    }
    Pizzas.push_back(pizza);
}

const Pizza &Pizzeria::FindPizza(const string &name) const {
    for (unsigned int i = 0; i < Pizzas.size(); i++){
        if (name == Pizzas[i].Name){
            return Pizzas[i];
        }
    }
    throw runtime_error("Pizza not found");
}

unsigned int Pizzeria::numNextOrder = 1000;

unsigned int Pizzeria::CreateOrder(const vector<string> &pizzas) {
    if (pizzas.size() == 0){
        throw runtime_error("Empty order");
    }
    Order order;
    for (unsigned int i = 0; i < pizzas.size(); i++){
        order.AddPizza(FindPizza(pizzas[i]));
    }
    Orders.insert({numNextOrder, order});
    numNextOrder++;
    return numNextOrder - 1;
}

const Order &Pizzeria::FindOrder(const unsigned int &numOrder) const {
    std::unordered_map<unsigned int, Order>::const_iterator got = Orders.find(numOrder);
    if (got == Orders.end()){
        throw runtime_error("Order not found");
    }
    return got->second;
}

string Pizzeria::GetReceipt(const unsigned int &numOrder) const {
    const Order& order = FindOrder(numOrder);
    string receipt = "";
    for (unsigned int i = 0; i < order.NumPizzas(); i++){
        //"- " + name + ", " + price + " euro" + "\n" " TOTAL: " + total + " euro" + "\n"
        receipt += "- " + order.GetPizza(i).Name + ", " + to_string(order.GetPizza(i).ComputePrice()) + " euro\n";
    }
    receipt += "  TOTAL: " + to_string(order.ComputeTotal()) + " euro\n";
    return receipt;
}

string Pizzeria::ListIngredients() const {
    string listIngredients = "";
    for (list<Ingredient>::const_iterator it = Ingredients.begin(); it != Ingredients.end(); it++){
        const Ingredient& ingredient = *it;
        listIngredients += ingredient.Name + " - '" + ingredient.Description + "': " + to_string(ingredient.Price) + " euro\n";
    }
    return listIngredients;
}

string Pizzeria::Menu() const {
    string menu = "";
    for (int i = Pizzas.size() - 1; i >= 0; i--){
        const Pizza& pizza = Pizzas[i];
        menu += pizza.Name + " (" + to_string(pizza.NumIngredients()) + " ingredients): " + to_string(pizza.ComputePrice()) + " euro\n";
    }
    return menu;
}

}
