#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      string Description;
      unsigned int Price;

      Ingredient(const string& name, const string& description, const unsigned int& price);
  };

  class Pizza {
    private:
      vector<Ingredient> Ingredients;

    public:
      const string Name;

      Pizza(const string& name, unsigned int size = 8);
      void AddIngredient(const Ingredient& ingredient);
      unsigned int NumIngredients() const;
      unsigned int ComputePrice() const;
  };

  class Order {
    private:
      vector<Pizza> Pizzas;

    public:
      void InitializeOrder(unsigned int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const unsigned int& position) const;
      unsigned int NumPizzas() const;
      unsigned int ComputeTotal() const;
  };

  class Pizzeria {
    private:
      static unsigned int numNextOrder;

      list<Ingredient> Ingredients;
      vector<Pizza> Pizzas;
      unordered_map<unsigned int, Order> Orders;

    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      unsigned int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const unsigned int& numOrder) const;
      string GetReceipt(const unsigned int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
